def good_practice(rand_array):
    bigger_than_fifties = [*rand_array[rand_array > 50]] # using masking, broadcasting and unpacking over an np.array
    return bigger_than_fifties
